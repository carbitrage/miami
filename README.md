# Miami

Miami is a **REST API** that provides all routes that needed for frontend part (Hollywood project).

## Installation
Clone current repository and install required packages via `npm install`. After that, execute `npm run build` to compile TypeScript into JavaScript.

After build, configure environment variables and then execute `npm start`.

## Configuration
Make a copy of `.env.example` file and rename it to `.env` and then open it.

### Environment variables
- **TEST_UID** - User ID of test user. This user must be available in database too.
- **TEST_PASSWORD** - password for test user. This entry should also be available in database.
- **API_HOST** - defines what network interface you want to use to start your server. It's **0.0.0.0** by default so Node.js would start listening using IPv4 interface.
- **API_PORT** - network port.
- **UID_LENGTH** - set to 9 by default because of length of chat IDs provided by Telegram API. This value is here just in case if there're gonna be changes against ids in Telegram API.
- **TG_TOKEN** - token for your Telegram Bot, contact @BotFather in Telegram to get one.
- **DB_HOST** - network hostname (or IP address) of your database location.
- **DB_PORT** - network port.
- **DB_NAME** - database name.
- **DB_USER** - database user.
- **DB_PASSWORD** - database password.
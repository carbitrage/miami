function createError(msg: string): CommonResponse {
    return {
        code: "Error",
        message: msg
    } as CommonResponse;
}

function createSuccess(msg: string): CommonResponse {
    return {
        code: "Success",
        message: msg
    } as CommonResponse;
}

function prepareSignalsMessage(signals: SpecificSignal[] | AnySignal[] | null): Object {
    return {
        code: "Success",
        message: signals
    } as Object;
}

export { createError, createSuccess, prepareSignalsMessage };
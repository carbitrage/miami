import { Client, QueryResult } from "pg";
import 'dotenv';

async function checkIfUserExists(uid: string): Promise<boolean> {
    return new Promise(resolve => {
        const client: Client = new Client({
            user: process.env.DB_USER, 
            host: process.env.DB_HOST, 
            port: +process.env.DB_PORT!, 
            database: process.env.DB_NAME, 
            password: process.env.DB_PASSWORD
        });
        client.connect();
        client.query(`SELECT uid FROM users WHERE uid = '${uid.toString()}';`)
            .then(function(response: QueryResult) {
                client.end();
                if (response.rowCount > 0) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            })
            .catch((error: Error) => {
                console.error(`Database ${error.stack}`);
            });
    });
}

async function writePassword(uid: number, password: string): Promise<void> {
    return new Promise(resolve => {
        const date: string = new Date().toISOString();
        const client: Client = new Client({
            user: process.env.DB_USER, 
            host: process.env.DB_HOST, 
            port: +process.env.DB_PORT!, 
            database: process.env.DB_NAME, 
            password: process.env.DB_PASSWORD
        });
        client.connect();
        client.query(`UPDATE users SET password = '${password}', p_create_time = '${date}' WHERE uid = '${uid.toString()}';`)
            .then(function() {
                client.end();
                resolve();
            })
            .catch((error: Error) => {
                console.error(`Database ${error.stack}`);
            });
    });
}

async function checkIfPasswordIsCorrect(request: AuthRequest): Promise<string> {
    return new Promise(resolve => {
        const client: Client = new Client({
            user: process.env.DB_USER, 
            host: process.env.DB_HOST, 
            port: +process.env.DB_PORT!, 
            database: process.env.DB_NAME, 
            password: process.env.DB_PASSWORD
        });
        client.connect();
        client.query(`SELECT uid, p_create_time FROM users WHERE uid = '${request.uid.toString()}' and password = '${request.password}';`)
            .then(function(response: QueryResult) {
                client.end();
                if (response.rowCount === 1) {
                    const data: AuthResponse[] = response.rows;
                    const timeZone: number = new Date().getTimezoneOffset();
                    const timeNow: number = +(new Date());
                    const timeWas: number = Date.parse(data[0].p_create_time) - (timeZone * 60000);
                    const difference: number = (timeNow - timeWas) / 60000;
                    if (difference < 15) {
                        resolve('ok');
                    } else {
                        resolve('expired');
                    }
                } else {
                    resolve('wrong');
                }
            })
            .catch((error: Error) => {
                console.error(`Database ${error.stack}`);
            });
    });
}

async function writeToken(uid: number, token: string): Promise<void> {
    return new Promise(resolve => {
        const date: string = new Date().toISOString();
        const client: Client = new Client({
            user: process.env.DB_USER, 
            host: process.env.DB_HOST, 
            port: +process.env.DB_PORT!, 
            database: process.env.DB_NAME, 
            password: process.env.DB_PASSWORD
        });
        client.connect();
        client.query(`UPDATE users SET token = '${token}', t_create_time = '${date}' WHERE uid = '${uid.toString()}';`)
            .then(function() {
                client.end();
                resolve();
            })
            .catch((error: Error) => {
                console.error(`Database ${error.stack}`);
            });
    });
}

async function removePassword(uid: number): Promise<void> {
    return new Promise(resolve => {
        const client: Client = new Client({
            user: process.env.DB_USER, 
            host: process.env.DB_HOST, 
            port: +process.env.DB_PORT!, 
            database: process.env.DB_NAME, 
            password: process.env.DB_PASSWORD
        });
        client.connect();
        client.query(`UPDATE users SET password = NULL, p_create_time = NULL WHERE uid = '${uid.toString()}';`)
            .then(function() {
                client.end();
                resolve();
            })
            .catch((error: Error) => {
                console.error(`Database ${error.stack}`);
            });
    });
}

async function removeToken(uid: number): Promise<void> {
    return new Promise(resolve => {
        const client: Client = new Client({
            user: process.env.DB_USER, 
            host: process.env.DB_HOST, 
            port: +process.env.DB_PORT!, 
            database: process.env.DB_NAME, 
            password: process.env.DB_PASSWORD
        });
        client.connect();
        client.query(`UPDATE users SET token = NULL, t_create_time = NULL WHERE uid = '${uid.toString()}';`)
            .then(function() {
                client.end();
                resolve();
            })
            .catch((error: Error) => {
                console.error(`Database ${error.stack}`);
            });
    });
}

async function getUserID(token: string): Promise<number | null> {
    return new Promise(resolve => {
        const client: Client = new Client({
            user: process.env.DB_USER, 
            host: process.env.DB_HOST, 
            port: +process.env.DB_PORT!, 
            database: process.env.DB_NAME, 
            password: process.env.DB_PASSWORD
        });
        client.connect();
        client.query(`SELECT uid FROM users WHERE token = '${token}';`)
            .then(function(response: QueryResult) {
                client.end();
                if (response.rowCount === 1) {
                    const uid: number = response.rows[0]['uid'];
                    resolve(uid);
                } else {
                    resolve(null);
                }
            })
            .catch((error: Error) => {
                console.error(`Database ${error.stack}`);
            });
    });
}

async function checkForNumberOfSignals(uid: number): Promise<boolean> {
    return new Promise(resolve => {
        const client: Client = new Client({
            user: process.env.DB_USER, 
            host: process.env.DB_HOST, 
            port: +process.env.DB_PORT!, 
            database: process.env.DB_NAME, 
            password: process.env.DB_PASSWORD
        });
        client.connect();
        client.query(`SELECT id FROM signals WHERE uid = '${uid.toString()}';`)
            .then(function(response: QueryResult) {
                client.end();
                if (response.rowCount >= 3) {
                    resolve(false);
                } else {
                    resolve(true);
                }
            })
            .catch((error: Error) => {
                console.error(`Database ${error.stack}`);
            });
    });
}

async function createSignal(uid: number, data: CreateSignalRequest): Promise<number | null> {
    return new Promise(resolve => {
        const markets: string[] = data.markets.map(function(market: string) {
            return `'${market}'`;
        }).sort();
        const query: string = `INSERT INTO signals(uid, pair, markets, difference, sent) 
        VALUES (${uid.toString()}, '${data.pair}', ARRAY[${markets}], ${data.difference.toString()}, FALSE) 
        RETURNING id;`;
        const client: Client = new Client({
            user: process.env.DB_USER, 
            host: process.env.DB_HOST, 
            port: +process.env.DB_PORT!, 
            database: process.env.DB_NAME, 
            password: process.env.DB_PASSWORD
        });
        client.connect();
        client.query(query)
            .then(function(response: QueryResult) {
                client.end();
                if (response.rowCount === 1) {
                    resolve(response.rows[0]['id']);
                } else {
                    resolve(null);
                }
            })
            .catch((error: Error) => {
                console.error(`Database ${error.stack}`);
            });
    });
}

async function getAllSignals(uid: number): Promise<AnySignal[] | []> {
    return new Promise(resolve => {
        const query: string = `SELECT id, pair, markets, difference FROM signals WHERE uid=${uid.toString()};`;
        const client: Client = new Client({
            user: process.env.DB_USER, 
            host: process.env.DB_HOST, 
            port: +process.env.DB_PORT!, 
            database: process.env.DB_NAME, 
            password: process.env.DB_PASSWORD
        });
        client.connect();
        client.query(query)
            .then(function(response: QueryResult) {
                client.end();
                if (response.rowCount === 0) {
                    resolve([]);
                } else if (response.rowCount < 4 && response.rowCount > 0) {
                    resolve(response.rows as AnySignal[]);
                }
            })
            .catch((error: Error) => {
                console.error(`Database ${error.stack}`);
            });
    });
}

async function getSpecificSignals(uid: number, params: GetSignalsParams): Promise<SpecificSignal[] | []> {
    return new Promise(resolve => {
        const pair: string = params.pair;
        const markets: string[] = params.markets.split(',').map(function(market: string) {
            return `'${market}'`;
        }).sort();
        const query: string = `SELECT id as sid, difference as value, difference as label FROM signals 
        WHERE uid=${uid.toString()} and pair='${pair}' and markets=ARRAY[${markets}];`;
        const client: Client = new Client({
            user: process.env.DB_USER, 
            host: process.env.DB_HOST, 
            port: +process.env.DB_PORT!, 
            database: process.env.DB_NAME, 
            password: process.env.DB_PASSWORD
        });
        client.connect();
        client.query(query)
            .then(function(response: QueryResult) {
                client.end();
                if (response.rowCount === 0) {
                    resolve([]);
                } else if (response.rowCount < 4 && response.rowCount > 0) {
                    resolve(response.rows as SpecificSignal[]);
                }
            })
            .catch((error: Error) => {
                console.error(`Database ${error.stack}`);
            });
    });
}

async function deleteSignal(uid: number, sid: number): Promise<boolean> {
    return new Promise(resolve => {
        const query: string = `DELETE FROM signals WHERE id=${sid.toString()} and uid=${uid.toString()} RETURNING id;`;
        const client: Client = new Client({
            user: process.env.DB_USER, 
            host: process.env.DB_HOST, 
            port: +process.env.DB_PORT!, 
            database: process.env.DB_NAME, 
            password: process.env.DB_PASSWORD
        });
        client.connect();
        client.query(query)
            .then(function(response: QueryResult) {
                client.end();
                if (response.rowCount === 1) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            })
            .catch((error: Error) => {
                console.error(`Database ${error.stack}`);
            });
    });
}

export { 
    checkIfUserExists, 
    writePassword, 
    checkIfPasswordIsCorrect, 
    writeToken, 
    removePassword, 
    removeToken, 
    getUserID, 
    checkForNumberOfSignals,
    createSignal,
    getAllSignals,
    getSpecificSignals,
    deleteSignal
};
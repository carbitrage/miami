declare interface CommonResponse {
    code: string;
    message: string;
}

declare interface AuthResponse {
    uid: number;
    p_create_time: string;
}

declare interface AllSignalsResponse {
    code: string;
    message: AnySignal[];
}

declare interface SpecificSignalsResponse {
    code: string;
    message: SpecificSignal[];
}